# Kali i3-gaps | Custom ISO
## Introduction
This is my take on Kali, using i3-gaps as a WM. I've done this because I loved
how i3-gaps worked on my new (well, it was new back in May 2019) laptop. It's
really lightweight (compared to other WM's and DE's) while being really
customizable and easy to use.

## Pictures

![Lockscreen][Lockscreen]

![Workspace 1][Workspace 1]

![Workspace 2][Workspace 2]

![Workspace 3][Workspace 3]

## Building
### Kali Debian Based Systems
To build the ISO:

- Install the dependencies,
- Clone the repo.
- `cd kali-i3_gaps` and start the build script.

```
$ sudo apt install -y curl git live-build cdebootstrap
$ git clone https://gitlab.com/Arszilla/kali-i3_gaps.git
$ cd kali-i3_gaps
$ ./build.sh --variant i3_gaps --verbose
```

### Non-Kali Debian Based Systems
If you are on a non-Kali Debian based system, check out Kali's
[guide][Kali docs] on building the ISO.

**Once the script is finished, your image should be in
`~/kali-i3_gaps/images`.**

## Bug Reports
I've been working on this since late November. There might be some errors here
and there; bugs and/or features from official Kali I've missed. If that's the
case and you find a bug and/or a missing feature:

- Fork the repo
- Make changes
- Submit a PR

I'll evaluate the changes ASAP and get back to you.

## Keybinds
This is the most important part about this ISO and i3-gaps in general. Unless
you are familiar with i3-gaps, you might be confused on how to do anything on
it. For the sake of newcomers, most vital keybinds are as follows:

- Mod key: Windows (Between Ctrl & Alt, usually)
- Launch a `urxvt`: `Mod + Return`
- Launch `rofi`: `Mod + d`
- Launch Firefox: `Mod  +F1`
- Launch a file `pcmanfm` file manager: `Mod + F2`
- Launch a file `ranger` file manager: `Mod + F3`
- Switch workspaces: `Mod + 1-9`
- Move the current window to another workspace: `Mod + Shift + 1-9`
- Launch the system menu: `Mod + 0`
- Switch between tiling and floating windows: `Mod + Shift + Space`
- Move the window: `Mod + Shift` and arrow keys or `jkl;`
- Resize the window: `Mod + R` then arrow keys or `jkl;`
- Take a fullscreen screenshot: `Mod + Print`
- Take a screenshot of a window: `Mod + Shift + Print`
- Take a screenshot of an area: `Mod + Ctrl + Print`

Rest of the keybinds could be found in `~/.config/i3/config`

## Customization
i3 and other packages used in this tiling WM are *really* customizable, which
is one of the perfect aspects of it IMO. A summary on customization is as
follows:

- `~/.config` holds the majority of the dotfiles/Customization
- `~/.config/i3/config` is i3-gaps' dotfile. You can change keybinds, execs,
look and feel as well as many more aspects of your i3-gaps here. You can make
your containers open in certain workspaces. For more information, check out
[i3wm docs][i3wm docs].
- `~/.config/i3status` is the i3status dotfile. You can customize your bar to
your liking. For more information, check out
[i3status docs][i3status docs].
- `~/.Xresources` is your Xresources file, where you can customize your X
client applications. This includes `urxvt` and `rofi`.
- `~/.config/compton/compton.conf` is the compton config. You can edit your
compositor's settings here.
- `~/.config/dunst/dunstrc` is the dunst dotfile. You can customize your
notification delivery system here.
- `~/.config/mimeapps.list` can be used to set the system-wide default
programs. By default, the system uses `vim` as a text editor, `sxiv` as an
image viewer, and Firefox as a default browser. See
`Default for foreign programs (user-speific)` in [this][Debian docs] page for
more information.
- `lxappearance` can be used to customize your system-wide theme, font, look,
and more.

## FAQ
- To connect to a WiFi, you can either use `nmtui` in your terminal or use
`nm-applet` in your `i3bar`.
- To edit your power options, search `xfce4-power-manager` in `rofi`.
- To manage your current display or multiple monitors, search `arandr` in
`rofi`.

## Known Issues
- In Live System instances there is no wallpaper present.
- Once the ISO is built and ran/installed, there won't be user directories (in 
`~` or in `root`). This is because `xdg-user-dirs-update` won't run (during 
build, live-system or install phase). A manual fix for this is to run 
`xdg-user-dirs-update` in a terminal once the you log into the system.

## Legal
This project uses the following projects with possible modifications:
- [multilockscreen][multilockscreen Github] by [jeffmhubbard][jeffmhubbard] and
[contributors][multilockscreen contributors]. Notable modifications are as
  follows:
  - `multilockscreen` has been installed to `/usr/bin` instead of
  `$HOME/.local/bin`. As a result, `$HOME/.local/bin/` is not added to the
  `PATH`
- [compton][compton Github] by [Christopher Jeffrey][Christopher Jeffrey] and
[contributors][compton contributors]. Notable modifications are as follows:
    - A basic `compton` config has been provided to allow smooth transitions
    between activities such as opening new containers or switching between
    workspaces.
- [dunst][dunst Github] by [Sascha Kruse][Sascha Kruse] and
[contributors][dunst contributors]. Notable modifications are as follows:
    - The default config has been modified to be more minimalistic and match
    Kali's 2020.1 appearance.  
- [i3-gaps][i3-gaps Github] by [Airblader][Airblader] and
[contributors][i3-gaps contributors]. Notable modifications are as follows:
  - The `i3`'s config has been modified to allow new users to adapt easily.
  - `i3bar` has been modified to match Kali's 2020.1 appearance as well as look
  simpler while being more informative.
- [i3lock-color][i3lock-color Github] by [PandorasFox][PandorasFox] and
[contributors][i3lock-color contributors].

## Acknowledgements
- [TJNull][TJNull] for encouraging me to do this, as well
as helping me and being an amazing friend.
- [Raphaël Hertzog][Raphaël Hertzog], [g0t mi1k][g0t mi1k],
[Sophie Brun][Sophie Brun], [Mati Aharoni][Mati Aharoni] and
[Devon Kearns][Devon Kearns] for their work on
[live-build-config][live-build-config], making it possible to make this ISO
happen.

[Lockscreen]:                   /Pictures/Lockscreen.png
[Workspace 1]:                  /Pictures/Workspace%201.png
[Workspace 2]:                  /Pictures/Workspace%202.png
[Workspace 3]:                  /Pictures/Workspace%203.png
[Kali docs]:                    https://www.kali.org/docs/development/live-build-a-custom-kali-iso/
[i3wm docs]:                    https://i3wm.org/docs/userguide.html
[i3status docs]:                https://i3wm.org/i3status/manpage.html
[Debian docs]:                  https://wiki.debian.org/DefaultWebBrowser
[i3-gaps Github]:               https://github.com/Airblader/i3
[Airblader]:                    https://github.com/Airblader
[i3-gaps contributors]:         https://github.com/Airblader/i3/graphs/contributors
[dunst Github]:                 https://github.com/dunst-project/dunst
[Sascha Kruse]:                 https://github.com/knopwob
[dunst contributors]:           https://github.com/dunst-project/dunst/graphs/contributors
[compton Github]:               https://github.com/chjj/compton
[Christopher Jeffrey]:          https://github.com/chjj
[compton contributors]:         https://github.com/chjj/compton/graphs/contributors
[i3lock-color Github]:          https://github.com/PandorasFox/i3lock-color/
[PandorasFox]:                  https://github.com/PandorasFox
[i3lock-color contributors]:    https://github.com/PandorasFox/i3lock-color/graphs/contributors
[multilockscreen Github]:       https://github.com/jeffmhubbard/multilockscreen/
[jeffmhubbard]:                 https://github.com/jeffmhubbard
[multilockscreen contributors]: https://github.com/jeffmhubbard/multilockscreen/graphs/contributors
[TJNull]:                       https://twitter.com/TJ_Null
[Raphaël Hertzog]:              https://twitter.com/raphaelhertzog
[g0t mi1k]:                     https://twitter.com/g0tmi1k
[Sophie Brun]:                  https://gitlab.com/sophiebrun
[Mati Aharoni]:                 https://twitter.com/muts
[Devon Kearns]:                 https://twitter.com/dookie2000ca
[live-build-config]:            https://gitlab.com/kalilinux/build-scripts/live-build-config
